package com.cpt202.team.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cpt202.team.models.Team;



//Spring  Aotation
@RestController
@RequestMapping("/team")
public class TeamController {
    private List<Team> teams = new ArrayList<Team>();

    @GetMapping("/team/list")
    public List<Team> getlist(){
        return teams;
    }


    @PostMapping("/team/add")
    public void addTeam(@RequestBody Team team){
        teams.add(team);
    }

    //JSON
    //{"name":"Jason",
    //"memberCount": 6}

}
